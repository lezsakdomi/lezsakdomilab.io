#!/usr/bin/env fish
for f in (ls media); ffmpeg -i media/$f -vf scale=640:-1 -vframes 1 -f image2 -y thumbnails/$f.jpg; end
