document.addEventListener('DOMContentLoaded', () => {
	for (let searchBar of document.querySelectorAll('input[type="search"][data-search-selector][data-search-properties]')) {
		const searchFunction = () => {
			const searchString = searchBar.value
			const searchRegExp = new RegExp(searchString, 'i')

			const {
				searchSelector,
				searchProperties: searchPropertiesString,
			} = searchBar.dataset
			const searchProperties = JSON.parse(searchPropertiesString)

			for (const element of document.querySelectorAll(searchSelector)) {
				// noinspection JSUnusedLocalSymbols
				element.style.display =
					// Should we hide?
					(searchString && Object.entries(element.dataset)
							.filter(([k, v]) => searchProperties.includes(k))
							.map(([k, v]) => v)
							.every(s => !s.match(searchRegExp))
					)
						? "none"
						: ""
			}
		}

		searchBar.addEventListener('input', searchFunction)
		if (searchBar.value) searchFunction()
	}
})
