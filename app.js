const express = require('express')
const stylus = require('stylus')

const app = express()

const port = process.env.PORT || 3000
app.set('view engine', 'pug')

const pages = app.locals.pages = [...require('./pages.json')]

app.use('/assets', express.static('assets'))

app.use('/scripts', express.static('scripts')) // todo use Babel or something

app.use('/stylesheets', stylus.middleware({
	src: 'stylesheets',
	dest: 'compiled/stylesheets',
	force: true,
	serve: false, // express.static used
}), express.static('compiled/stylesheets'))

for (const page of pages) {
	const {url: urls, title: titles, view} = page
	for (const lang of Object.getOwnPropertyNames(urls)) {
		const url = urls[lang]
		const title = titles && titles[lang]

		const path = pages.filter(page => url.startsWith(page.url[lang]))
		path.sort((a, b) => a.url[lang].length - b.url[lang].length)

		app.get(encodeURI(url), (req, res) => {
			res.render(view, {lang, url, path, page})
		})
	}
}

app.get('/', (req, res, next) => {
	try {
		const page = pages.filter(page => page.view === 'home')[0]

		if (page.url["hu"] && req.acceptsLanguages().some(l => l === "hu" || l === "hu-HU")) {
			res.redirect(page.url["hu"])
			return
		}

		for (const lang of Object.getOwnPropertyNames(page.url)) {
			if (req.acceptsLanguages().some(l => l === lang)) {
				res.redirect(page.url[lang])
				return
			}
		}

		// fallback to english
		res.redirect(page.url["en"])
	} catch (e) {
		next(e)
	}
})

app.get('/test', (req, res) => {
	res.render('test')
})

app.listen(port)
